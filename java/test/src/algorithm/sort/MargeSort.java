package algorithm.sort;

public class MargeSort extends Sort {

  public MargeSort(int[] nums) {
    super(nums);
  }

  @Override
  public void sort() {
    int[] sortedNumbers = sort_(m_numbers);
    for (int i = 0; i < sortedNumbers.length; i++) {
    	m_numbers[i] = sortedNumbers[i];
    }
  }

  private int[] sort_(int[] nums) {
    // Stop condition.
    if (nums.length == 1) return nums;

    // Devides the array into two which has same size.
    int middle = nums.length / 2;
    int[] a = new int[middle];
    int[] b = new int[nums.length - middle];
    for (int i = 0; i < middle; i++) {
      a[i] = nums[i];
    }
    for (int j = middle; j < nums.length; j++) {
      b[j - middle] = nums[j];
    }
    return marge_(sort_(a), sort_(b));
  }

  private int[] marge_(int[] a, int[] b) {
    int[] result = new int[a.length + b.length];
    
    int aI = 0;
    int bI = 0;
    for (int i = 0; i < result.length; i++) {
      if (aI >= a.length){
        result[i] = b[bI];
        bI++;
      } else if (bI >= b.length) {
        result[i] = a[aI];
        aI++;
      } else if (a[aI] < b[bI]) {
        result[i] = a[aI];
        aI++;
      } else {
        result[i] = b[bI];
        bI++;
      }
    }
    
    return result;
  }

}
