package algorithm.sort;

public class QuickSort extends Sort {

  public QuickSort(int[] nums) {
    super(nums);
  }

  @Override
  public void sort() {
    sort_(0, m_numbers.length - 1);
  }

  private void sort_(int low, int high) {
    int pivot;
    if (high > low) {
      pivot = partition(low, high);
      sort_(low, pivot - 1);
      sort_(pivot + 1, high);
    }
  }

  private int partition(int low, int high) {
    int left, right, pivotItem = m_numbers[low];
    left = low;
    right = high;

    while (left < right) {

      while (left < high && m_numbers[left] <= pivotItem) left++;
      while (right > low && m_numbers[right] > pivotItem) right--;

      if (left < right) {
        int temp = m_numbers[left];
        m_numbers[left] = m_numbers[right];
        m_numbers[right] = temp;
      }
    }

    m_numbers[low] = m_numbers[right];
    m_numbers[right] = pivotItem;
    return right;
  }

}
