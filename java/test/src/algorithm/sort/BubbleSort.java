package algorithm.sort;

public class BubbleSort extends Sort {
	
	public BubbleSort(int[] nums) {
		super(nums);
	}
	
	@Override
	public void sort() {
		// Outer loop - need n-1 iteration to sort n elements
        for(int i=0; i<m_numbers.length -1; i++){
            for(int j= 1; j<m_numbers.length -i; j++){
              
                //If current number is greater than swap those two
                if(m_numbers[j-1] > m_numbers[j]){
                    int temp = m_numbers[j];
                    m_numbers[j] = m_numbers[j-1];
                    m_numbers[j-1] = temp;
                }
            }
        }
	}
}
