package algorithm.sort;

public class Sort {
	protected int [] m_numbers = null;
	
	protected Sort(int[] nums) {
		m_numbers = nums.clone();
	}
	
	public void sort() {
		
	}
	
	public boolean isSorted() {
		boolean sorted = true;
		for(int i= 1; i<m_numbers.length; i++) {
			if(m_numbers[i-1] > m_numbers[i]) {
				sorted = false;
				break;
			}
		}
		return sorted;
	}
	
	public int[] getNums()
	{
		return m_numbers;
	}
}
