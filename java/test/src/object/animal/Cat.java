package object.animal;

/*
 * @OverrideでAnimalクラスのメソッドを上書き
 */
public class Cat extends Animal {
	public Cat() {
		m_name 	= "ねこ";
		m_cry 		= "にゃんにゃん";
	}
	
	@Override
	public void introduce()
	{
		System.out.println(m_name + "だからしゃべれるわけないやん！");
	}
	
}
