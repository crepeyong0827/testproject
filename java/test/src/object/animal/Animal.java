package object.animal;


/**
 * 生き物のクラス
 */

/*
 * 	private ここでしか使えない
 * protected 継承すればつかえる
 * public	どこでも使える
 */

public class Animal {
	protected String m_name	= "";			// 名前
	protected String m_cry		= "";			// 鳴き声
	
	public Animal () {
	}
	
	// 泣く
	public void cry()
	{
		System.out.println(m_name+":" +m_cry);
	}
	
	// 自己紹介
	public void introduce()
	{
		System.out.println("私の名前は" + m_name + "です。");
	}
}
