package com.popcorn.quizMaker;

public class QMMain {
	
	/**
	 * 	@memo
	 * ■処理の流れ
	 * 		1.初期化
	 * 		2.メインループ
	 * 		3.出力処理
	 * 		4.入力処理
	 */
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		// ********************************************
		//	定数
		// ********************************************
		
		// ********************************************
		//	変数
		// ********************************************
		boolean bLoopFlg = true;	// ループフラグ
		int inputNum = 0;	// 入力番号
		
		// ********************************************
		//	メイン処理
		// ********************************************
		while(bLoopFlg) {
			////////////////////////////////////////////////////////////////////////
			// 実行と描画処理
			
			// テスト用見出し
			System.out.println("入力してください>");
			
			////////////////////////////////////////////////////////////////////////
			//キー入力処理　そのまま書き写してください　ここから	 
			int tmpInputNum = 0;	//入力番号初期化	 
			try {	 
				//初期化処理	 
				final int IMPUT_MAX = 3;	//最大入力値	 
				//キー入力読込処理（int型）	 
				java.util.Scanner sc = new java.util.Scanner(System.in);	 
				int inputInt = sc.nextInt();	 // 入力があるまで停止して待つ。
				//入力値チェックと入力番号への代入	
				if (inputInt > 0 && inputInt <= IMPUT_MAX) {	 
					tmpInputNum = inputInt;	 
				} else {	 
					System.out.println("※　コマンドは" + IMPUT_MAX + "以下で入力して下さい　※　");	 
				}
			} catch (Exception e) {	 
				System.out.println("※　数字以外は入力しないで下さい　※　");	 
			}	 
			//キー入力処理　そのまま書き写してください　ここまで	 
			////////////////////////////////////////////////////////////////////////
			
			//入力番号を受け取る	 
			inputNum = tmpInputNum;	 
			
			//テスト用：入力結果表示	 
			System.out.println("入力された値は　" + inputNum + "です。");	 
			
			//表示終了の区切り	 
			System.out.println("");	 
			System.out.println("######################################################");	 
			System.out.println("");
			
		}
	}

}
