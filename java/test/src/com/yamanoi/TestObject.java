package com.yamanoi;

import object.animal.Animal;
import object.animal.Cat;
import object.animal.Dog;

public class TestObject {
	
	public static void main(String args[]) {
		Animal ikimono1 = new Cat();
		ikimono1.introduce();
		ikimono1.cry();
		
		Animal ikimono2 = new Dog();
		ikimono2.introduce();
		ikimono2.cry();
	}
}
