package com.yamanoi;

import java.util.Arrays;

import algorithm.sort.BubbleSort;
import algorithm.sort.MargeSort;
import algorithm.sort.QuickSort;
import algorithm.sort.Sort;


public class TestSort {
	protected Sort m_sorter;
	protected boolean m_showArray = true;
	
	public enum SortType {
		Sort_Quick,
		Sort_Bubble,
		Sort_Marge,
	}
	
	public static void main(String args[]) {
		TestSort ts = new TestSort();
		int[] numbers = TestSort.generateRandomArray(100);
		ts.testSort(TestSort.SortType.Sort_Bubble, numbers);
		ts.testSort(TestSort.SortType.Sort_Quick, numbers);
		ts.testSort(TestSort.SortType.Sort_Marge, numbers);
	}
	
	private Sort createSorter(SortType type, int[] numbers) {
		Sort sorter = null;
		switch(type) {
			case Sort_Quick: 	sorter = new QuickSort(numbers);	break;
			case Sort_Bubble: 	sorter = new BubbleSort(numbers);	break;
			case Sort_Marge:	sorter = new MargeSort(numbers);	break;
		}
		return sorter;
	}
	
	public void testSort(SortType type, int[] numbers)
	{
		m_sorter = createSorter(type, numbers);
		if(m_sorter == null) return;
		// ソート前
		long start = System.currentTimeMillis();
		System.out.printf("[%s]%n", m_sorter.getClass().getSimpleName());
		if(m_showArray) { 
			System.out.printf("Array before: %s %n", Arrays.toString(m_sorter.getNums()));
		}
		// ソート
		while(!m_sorter.isSorted()) {
			m_sorter.sort();
		}
		// ソート後
		long stop = System.currentTimeMillis();
		if(m_showArray) {
			System.out.printf("Array after: %s %n", Arrays.toString(m_sorter.getNums()));
		}
		System.out.printf("実行にかかった時間は " + (stop - start) + " ミリ秒です。%n");
	}
	
	static public  int[] generateRandomArray(int length) {
		int[] numbers = new int[length];
		for(int i=0; i<length;i++) {
			numbers[i] = (int) (Math.random() * length);
		}
		return numbers;
	}
}
